
package Table;


public class UserTableFrame extends javax.swing.JFrame {
   
     public UserTableFrame() {
        initComponents();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        userTable = new javax.swing.JTable();
        showUserbutton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        userTable.setModel(new UserTableModel());
        jScrollPane1.setViewportView(userTable);

        showUserbutton.setText("show");
        showUserbutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showUserbuttonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(640, 640, 640)
                .addComponent(showUserbutton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 470, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(showUserbutton))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void showUserbuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showUserbuttonActionPerformed
        showUserDetail();
    }//GEN-LAST:event_showUserbuttonActionPerformed

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(UserTableFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(UserTableFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(UserTableFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UserTableFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        Data.load();
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new UserTableFrame().setVisible(true);
            }
        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton showUserbutton;
    private javax.swing.JTable userTable;
    // End of variables declaration//GEN-END:variables

    private void showUserDetail() {
        System.out.println(userTable.getModel().getValueAt(userTable.getSelectedRow(),1));
        String username = userTable.getModel().getValueAt(userTable.getSelectedRow(),1).toString();
        openUserDeatail(username);
        }

    private void openUserDeatail(String username) {
        this.setVisible(false);
        this.dispose();
        UserDetailFrame frame = new UserDetailFrame(username);
        frame.setVisible(true);
    }
}
