/*
 
    20.12 Table+TableModel
 */
package Table;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

public class UserTableModel extends AbstractTableModel{
    String[] columnNames = {"id","username","name","surname"};
    ArrayList<User> userList = Data.userList;
    public UserTableModel(){

    
    }
    @Override
    public String getColumnName(int column) {
        return columnNames[column]; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getRowCount() {
        return userList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        User user = userList.get(rowIndex);
        if(user == null)return null;
        switch(columnIndex){
            case 0:return user.getId();
            case 1:return user.getLogin();
            case 2:return user.getName();
            case 3:return user.getSurname();
        }
        return "";
    }
    
}
